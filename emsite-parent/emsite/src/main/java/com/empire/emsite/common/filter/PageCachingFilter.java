/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.common.filter;

import net.sf.ehcache.CacheManager;
import net.sf.ehcache.constructs.web.filter.SimplePageCachingFilter;

import com.empire.emsite.common.utils.SpringContextHolder;

/**
 * 类PageCachingFilter.java的实现描述：页面高速缓存过滤器
 * 
 * @author arron 2017年10月30日 下午6:30:11
 */
public class PageCachingFilter extends SimplePageCachingFilter {

    private CacheManager cacheManager = SpringContextHolder.getBean(CacheManager.class);

    @Override
    protected CacheManager getCacheManager() {
        return cacheManager;
    }

}
