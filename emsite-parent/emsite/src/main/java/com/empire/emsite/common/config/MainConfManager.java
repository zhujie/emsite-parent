package com.empire.emsite.common.config;

import com.ckfinder.connector.ServletContextFactory;
import com.empire.emsite.common.utils.StringUtils;

/**
 * 类MainConfManager.java的实现描述：后台框架配置文件加载
 * 
 * @author arron 2018年1月8日 下午6:02:54
 */
public class MainConfManager {

    /**
     * 上传文件基础虚拟路径
     */
    public static final String USERFILES_BASE_URL = "/userfiles/";

    /**
     * 获取上传文件的根目录
     * 
     * @return
     */
    public static String getUserfilesBaseDir() {
        String dir = getConfig("userfiles.basedir");
        if (StringUtils.isBlank(dir)) {
            try {
                dir = ServletContextFactory.getServletContext().getRealPath("/");
            } catch (Exception e) {
                return "";
            }
        }
        if (!dir.endsWith("/")) {
            dir += "/";
        }
        //      System.out.println("userfiles.basedir: " + dir);
        return dir;
    }

    /**
     * 获取URL后缀
     */
    public static String getUrlSuffix() {
        return getConfig("urlSuffix");
    }

    /**
     * 获取前端根路径
     */
    public static String getFrontPath() {
        return getConfig("frontPath");
    }

    /**
     * 获取管理端根路径
     */
    public static String getAdminPath() {
        return getConfig("adminPath");
    }

    /**
     * 是否是演示模式，演示模式下不能修改用户、角色、密码、菜单、授权
     */
    public static Boolean isDemoMode() {
        String dm = getConfig("demoMode");
        return "true".equals(dm) || "1".equals(dm);
    }

    /**
     * 获取配置：是否允许多账号同时登录
     * 
     * @return
     */
    public static String getMultiAccountLogin() {
        return getConfig("user.multiAccountLogin");
    }

    /**
     * 获取配置：视图文件前缀
     * 
     * @return
     */
    public static String getWebViewPrefix() {
        return getConfig("web.view.prefix");
    }

    /**
     * 获取配置：是否不允许刷新主页，不允许情况下，刷新主页会导致重新登录
     * 
     * @return
     */
    public static String getNotAllowRefreshIndex() {
        return getConfig("notAllowRefreshIndex");
    }

    /**
     * 获取配置：产品名称
     * 
     * @return
     */
    public static String getProductName() {
        return getConfig("productName");
    }

    /**
     * 获取配置：静态文件后缀
     * 
     * @return
     */
    public static String getWebStaticFile() {
        return getConfig("web.staticFile");
    }

    /**
     * 获取配置：如果使用Cache，并且在Cache里存在，则直接返回。硕正树功能
     * 
     * @return
     */
    public static String getSupcanUseCache() {
        return getConfig("supcan.useCache");
    }

    /**
     * 获取配置
     * 
     * @see ${fns:getConfig('adminPath')}
     */
    public static String getConfig(String key) {
        String value = Global.getConfig(key);
        return value;
    }
}
